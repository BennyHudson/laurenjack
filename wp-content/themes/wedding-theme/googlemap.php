<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBR5bX6m_CEPwitun65XjrFWYZVRtzqADA&sensor=false&extension=.js"></script> 
<script> google.maps.event.addDomListener(window, 'load', init);

var map;

function init() {
    var mapOptions = {
        center: new google.maps.LatLng(51.197610,-0.843667),
        zoom: 14,
                zoomControl: false,
                zoomControlOptions: {
                    style: google.maps.ZoomControlStyle.DEFAULT,
                },
                disableDoubleClickZoom: true,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                },
                scaleControl: true,
                scrollwheel: false,
                streetViewControl: false,
                draggable : true,
                overviewMapControl: true,
                overviewMapControlOptions: {
                    opened: false,
                },
         mapTypeId: google.maps.MapTypeId.ROADMAP,
    
    
    }

    var mapElement = document.getElementById('map');
    var map = new google.maps.Map(mapElement, mapOptions);
    var locations = [
        ['GU10 5EU', 51.196787, -0.844660]
    ];

    for (i = 0; i < locations.length; i++) {
        marker = new google.maps.Marker({
            icon: '',
            position: new google.maps.LatLng(locations[i][1], locations[i][2]),
            map: map
        });
    }
}
</script> 