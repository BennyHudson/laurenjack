4// JavaScript Document

$(document).ready(function(){	
	$('.bxslider').bxSlider({
	  pager: false,
	  captions: true
	});
	$(function() {
		$('a[href*=#]:not([href=#])').click(function() {
			if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
				var target = $(this.hash);
				target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
					if (target.length) {
						$('html,body').animate({
					  		scrollTop: target.offset().top - 70
						}, 1000);
					return false;
				}
			}
		});
	});	
	$(".track-listing").hide();
		//Show the text field only when the third option is chosen - this doesn't
        $('#rsvp-select').change(function() {
            if ($("#rsvp-select").val() == "I/We will be attending") {
                    $(".track-listing").show();
            }
            else {
                    $(".track-listing").hide();
            }
    });
	$("#rsvp-select").prepend("<option value='' selected='selected' disabled>RSVP</option>");
})
$(window).scroll(function() {
	if ($(this).scrollTop() > 800) {
		 $('nav').addClass('mini-nav');
	} else {
		 $('nav').removeClass('mini-nav');
	}
});

var aChildren = $("nav a") // find the a children of the list items
var aArray = []; // create the empty aArray
    for (var i=0; i < aChildren.length; i++) {    
        var aChild = aChildren[i];
        var ahref = $(aChild).attr('href');
        aArray.push(ahref);
    } // this for loop fills the aArray with attribute href values
 
    $(window).scroll(function(){
        var windowPos = $(window).scrollTop(); // get the offset of the window from the top of page
        var windowHeight = $(window).height(); // get the height of the window
        var docHeight = $(document).height();
 
        for (var i=0; i < aArray.length; i++) {
            var theID = aArray[i];
            var divPos = $(theID).offset().top -70; // get the offset of the div from the top of page
            var divHeight = $(theID).height(); // get the height of the div in question
            if (windowPos >= divPos && windowPos < (divPos + divHeight)) {
                $("a[href='" + theID + "']").addClass("nav-active");
            } else {
                $("a[href='" + theID + "']").removeClass("nav-active");
            }
        }
 
        if(windowPos + windowHeight == docHeight) {
            if (!$("nav a:last-child").hasClass("nav-active")) {
                var navActiveCurrent = $(".nav-active").attr("href");
                $("a[href='" + navActiveCurrent + "']").removeClass("nav-active");
                $("nav a:last-child ").addClass("nav-active");
            }
        }
    });