<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/modernizr-latest.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width"/>  
<title><?php wp_title('|'); ?></title>
<?php wp_head(); ?>
</head>
<body>

	<header>
        <nav>
           	<a href="#story">Our Story</a> • <a href="#photos">Photos</a> • <a href="#party">The Wedding Party</a> • <a href="#venue">The Venue</a> • <a href="#getting-there">Getting there</a> • <a href="#where-to-stay">Where to stay</a> • <a href="#day">The Day</a> • <a href="#gifts">Gifts</a> • <a href="#rsvp">RSVP</a>
        </nav>
    </header>
    
    <section id="title">
        	<div id="mr-m" class="title-letter"></div>
            <div id="mr-r" class="title-letter"></div>
            <div id="mr-and" class="title-letter"></div>
            <div id="mrs-m" class="title-letter"></div>
            <div id="mrs-r" class="title-letter"></div>
            <div id="mrs-s" class="title-letter"></div>
    </section>
    
    <section id="intro">
       	<div class="ribbon">
        	<div class="ribbon-border">
            	<div class="ribbon-bottom">
                    <div class="container">
                        <h1>Lauren &amp; Jack’s big day</h1>
                        <h2>Friday, <span class="number">25</span>th July <span class="number">2014</span><br>Farnham, Surrey</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="story">
        <h2>Our Story</h2>       
        <div class="content-left col">
          	<h3>By Lauren</h3>
        </div>
        <div class="content-right col">
        	<h3>By Jack</h3>
        </div>
        <div class="clear"></div>
    	<div id="ss-container" class="ss-container">
        		<div class="ss-row jack-laps blank">
                
                </div>
                <div class="ss-row jack">
                    <div class="ss-right">
                        <div class="content-box">
                        	<h3>The first time we met (properly)</h3>
                            <p>September <span class="number">2003</span>. We were in the same class for AS English and I was sat between Lauren and one of her best friends (and bridesmaids), Kat. It wasn’t long before the incessant giggling and girly chat got too much for me and, after three weeks, I dropped out of English... Things got a lot better from this point!</p>
                        </div>
                    </div>
                </div>
                <div class="ss-row laps">
                    <div class="ss-left">
                        <div class="content-box">
                        	<h3>The first kiss</h3>
                            <p>New Year’s Eve <span class="number">2003</span><span class="number">. After one too many Bacardi Breezers, I was sick during the midnight fireworks all over our friend’s patio. The first kiss then happened soon after this. Now that’s love…</p>
                        </div>
                    </div>
                </div>
                <div class="ss-row jack">
                    <div class="ss-right">
                        <div class="content-box">
                        	<h3>Becoming ‘official’</h3>
                            <p>February <span class="number">2004</span>. Ages ago, making this year our <span class="number">10</span> year anniversary! </p>
                        </div>
                    </div>
                </div>
                <div class="ss-row laps">
                    <div class="ss-left">
                        <div class="content-box">
                        	<h3>Off to Uni</h3>
                            <p>September <span class="number">2006</span>. Jack and I started at the University of Southampton. Not only did we not realise we had both applied to the same Uni but we’re still not quite sure how Jack got in with his A Level grades... but it was obviously meant to be. </p>
                        </div>
                    </div>
                </div>
                <div class="ss-row jack">
                    <div class="ss-right">
                        <div class="content-box">
                        	<h3>Moving into <span class="number">235</span>A Fleet Road</h3>
                            <p>May <span class="number">2011</span>. Lauren and I took on our first project together as we moved into a flat that was in a horrendous state. We then spent the next three months decorating (with the help of our friends and families!) and now find our spare room and lounge full of hungover bodies on most Sunday mornings. </p>
                        </div>
                    </div>
                </div>
                <div class="ss-row laps">
                	<div class="ss-left">
                    	<div class="content-box">
                        	<h3>The proposal</h3>
                            <p>Beachside restaurant in Hua Hin, Thailand, May <span class="number">2013</span>. It was our last night in Thailand and Jack proposed over dinner. He gave me one of Argos’ £<span class="number">5</span> rings (which I instantly panicked about in case it was the real thing!) before Jack quickly reassured me that we would be choosing the right one when we arrived in Dubai the following day. </p>
                        </div>
                    </div>
                </div>
                <div class="ss-row jack-laps blank">
                
                </div>
            </div>             
        <div class="clear"></div>
    </section>
    
    <section id="photos">
    	<div class="container">
        	<div class="gallery-box">
            	<ul class="bxslider">
                	<li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-1.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-2.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-3.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-4.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-5.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-6.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-7.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-8.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-9.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-10.jpg"></li>
                    <li><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/gallery/image-11.jpg"></li>
                </ul>
            </div>
        </div>
    </section>
    
    <section id="party">
    	<div class="container">
        	<h2>The Wedding Party</h2>
            <div class="content-left col">
            	<h3>Bridesmaids</h3>
            </div>
            <div class="content-right col">
            	<h3>Groomsmen</h3>
            </div>
            <div class="clear"></div>
        </div>
        <div class="ss-container">
        	<div class="ss-row jack-laps blank">
                
            </div>
        	<div class="ss-row">
            	<div class="ss-left">
                    <div class="bridesmaid" id="george">
                    	<div class="object"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/george.jpg" alt="Georgina Byrne - Bridesmaid">
                    </div>
                </div>
                <div class="ss-right">
                	<div class="groomsman" id="chris">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/chris.jpg" alt="Christopher Barnes - Best Man">
                    </div>
                </div>
            </div>
            <div class="ss-row">
            	<div class="ss-left">
                    <div class="bridesmaid" id="kiri">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/kiri2.jpg" alt="Kiri Dixon - Bridesmaid">
                    </div>
                </div>
                <div class="ss-right">
                    <div class="groomsman" id="dave">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/dave.jpg" alt="David Jones - Best Man">
                    </div>
                </div>
            </div>
            <div class="ss-row">
            	<div class="ss-left">
                    <div class="bridesmaid" id="zoe">
                    	<div class="object"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/zoe.jpg" alt="Zoe Finn - Bridesmaid">
                    </div>
                </div>
                <div class="ss-right">
                	<div class="groomsman" id="morgan">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/morgan.jpg" alt="Morgan Applegarth - Usher">
                    </div>
                </div>
            </div>
            <div class="ss-row">
            	<div class="ss-left">
                    <div class="bridesmaid" id="vicky">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/vicky.jpg" alt="Vicky Hawker - Bridesmaid">
                    </div>
                </div>
                <div class="ss-right">
                    <div class="groomsman" id="joel">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/joel.jpg" alt="Joel Chambers - Usher">
                    </div>
                </div>
            </div>
            <div class="ss-row">
            	<div class="ss-left">
                    <div class="bridesmaid" id="kat">
                    	<div class="object"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/kat.jpg" alt="Katherine Paes - Bridesmaid">
                    </div>
                </div>
                <div class="ss-right">
                    <div class="groomsman" id="cash">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/al-c.jpg" alt="Alex Cash - Usher">
                    </div>
                </div>
            </div>
            <div class="ss-row">
            	<div class="ss-left">
                    <div class="bridesmaid" id="laura">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/laura.jpg" alt="Laura Whittington - Bridesmaid">
                    </div>
                </div>
                <div class="ss-right">
                    <div class="groomsman" id="ouzo">
                    	<div class="object"></div>
                        <div class="object2"></div>
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/party/al-o.jpg" alt="Alex Ouzonoglou - Usher">
                    </div>
                </div>
            </div>
            <div class="ss-row jack-laps blank">
                
            </div>            
        </div>
    </section>
    
    <section id="venue">
    	<div class="container">
        	<h2>The Venue</h2>
            <div class="col venue-left">
            	<div class="gallery-box">
                	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/venue.jpg" alt="Northbrook Park">
                </div>	
            </div>
            <div class="col venue-right">
            	<p>Northbrook Park<br>
                Farnham<br>
                Surrey<br>
                GU<span class="number">10 5</span>EU</p>
                <p><a href="http://www.northbrookpark.co.uk" target="_blank">northbrookpark.co.uk</a></p>
                <p><span class="number">08448 247772</span></p>
                <p><a href="mailto:info@northbrookpark.co.uk">info@northbrookpark.co.uk</a>
            </div>
            <div class="clear"></div>
        </div>
    </section>
    
    <section id="getting-there">
    	<div class="container">
        	<h2>Getting There</h2>
            <div class="gallery-box" id="map-box">
            	<?php include('googlemap.php'); ?>
                <div id="map"></div>
            </div>
        </div>
        <div class="directions">
        	<div class="container">
                <div class="content-left col">
                    <h3>From M<span class="number">25</span></h3>
                    <p>Leave the M<span class="number">25</span> at junction <span class="number">10</span> onto the A<span class="number">3</span> and follow the signs for Guildford. Remain on the A<span class="number">3</span> for <span class="number">9.8</span> miles. Then join the A<span class="number">31</span> signposted Farnham and Winchester. Follow signs for Farnham and Alton. At the Shepherd and Flock roundabout follow signs to Alton. At the next two traffic lights continue straight on towards Alton. At the next roundabout (Farnham Bypass) take the second exit remaining on the A<span class="number">31</span> towards Alton. After approx <span class="number">1.5</span> Miles turn right into Northbrook Park – signposted Northbrook.</p>
                </div>
                <div class="content-right col">
                    <h3>From M<span class="number">3</span></h3>
                    <p>Leave the M<span class="number">3</span> at junction <span class="number">10</span> following the signs for the A<span class="number">31</span> towards Alton and Farnham. Remain on the A<span class="number">31</span> for approximately <span class="number">22</span> miles towards Farnham. Before reaching the Farnham Bypass, turn left into Northbrook Park – signposted Northbrook.</p>
                </div>
                <div class="clear"></div>
            </div>
        </div>
    </section>
    
<section id="where-to-stay">
    	<div class="container">
        	<h2>Where To Stay</h2>
            <ul>
                <li><a href="http://www.bishopstable.com/" target="_blank">Bishops Table Hotel</a>, Farnham  (<span class="number">2.3</span> miles from venue)</li>
                <li><a href="http://www.mercure.com/gb/hotel-6621-mercure-farnham-bush-hotel/index.shtml" target="_blank">Mercure Farnham Bush Hotel</a>, Farnham (<span class="number">2.5</span> miles from venue)</li>
                <li><a href="http://www.1parkrow.com/" target="_blank"><span class="number">1</span> Park Row</a>, Farnham (<span class="number">2.7</span> miles from venue)</li>
                <li><a href="http://www.travelodge.co.uk/hotels/567/Aldershot-hotel" target="_blank">Travelodge</a>, Aldershot (<span class="number">6.3</span> miles from venue)</li>
            </ul>
            <p>As Northbrook Park is not within walking distance of Farnham town centre, it is advised that all guests book taxis in advance. Please <a href="#day">click here</a> to view the timings for the day.</p>
            <ul>
            	<li>Farnham Station Taxis – <span class="number">01252 735735</span></li>
                <li>Rushmoor Taxis – <span class="number">01252 333555</span></li>
                <li>Home James Taxis – <span class="number">01252 722296</span></li>
                <li>Anytime Cars – <span class="number">01252 494100</span></li>
                <li>A2B Taxis – <span class="number">01252 815000</span></li>
            </ul>
        </div>
    </section>
    
    <section id="day">
    	<div class="container">
    		<h2>The Day</h2> 
            <div class="ss-container">
                <div class="ss-row itinerary">
                    <div class="ss-right">
                    	<h3><span class="number">13:15 – 13:30</span></h3>
                        <p>Guests arrive at venue</p>
                    </div>
                </div>
                <div class="ss-row itinerary">
                    <div class="ss-right">
                    	<h3><span class="number">14:00</span></h3>
                        <p>Ceremony begins</p>
                    </div>
                </div>
                <div class="ss-row itinerary">
                    <div class="ss-right">
                    	<h3><span class="number">14:30</span></h3>
                        <p>Drinks reception, canapés and photos</p>
                    </div>
                </div>
                <div class="ss-row itinerary">
                    <div class="ss-right">
                    	<h3><span class="number">16:30</span></h3>
                        <p>Wedding breakfast begins</p>
                    </div>
                </div>
                <div class="ss-row itinerary">
                    <div class="ss-right">
                    	<h3><span class="number">19:30</span></h3>
                        <p>Evening guests arrive and party starts. Food will be served for all guests throughout the evening.</p>
                    </div>
                </div>
                <div class="ss-row itinerary">
                    <div class="ss-right">
                    	<h3><span class="number">23:30 – 00:00</span></h3>
                        <p>Carriages - all guests to leave the venue by midnight</p>
                    </div>
                </div>
            </div>  
        </div>
            <div class="clear"></div>
    </section>
    
    <section id="gifts">
    	<div class="ribbon">
            <div class="ribbon-border">
            	<div class="ribbon-bottom">
                    <div class="container">
                        <h2>Gifts</h2>
                        <p>As we already live together we won’t be asking you for a new kettle or toaster. Instead, we would love for anyone who does want to buy us a gift to do so in the form of a donation to our honeymoon, which will be a road trip through California.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <section id="rsvp">
    	<div class="container">
        	<h2>RSVP</h2>
            <?php echo do_shortcode('[contact-form-7 id="4" title="RSVP"]'); ?>
            <div class="clear"></div>
        </div>
    </section>
    
    <footer>
    	Website by <a href="http://thetwentysix.co.uk/" target="_blank" title="Website by The Twenty Six">The Twenty Six</a>
    </footer>
        
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/enquire.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/wedding.js" type="text/javascript"></script>
<?php wp_footer() ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47915841-1', 'laurenandjacksbigday.co.uk');
  ga('send', 'pageview');

</script>
</body>
</html>