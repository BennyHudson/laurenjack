<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/modernizr-latest.js" type="text/javascript"></script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="viewport" content="width=device-width"/>  
<title><?php wp_title('|'); ?></title>
<?php wp_head(); ?>
</head>
<body>

	<header>
        <nav>
           	<a href="/">Head back to the main site</a>
        </nav>
    </header>
    
    <section id="title">
        	<div id="mr-m" class="title-letter"></div>
            <div id="mr-r" class="title-letter"></div>
            <div id="mr-and" class="title-letter"></div>
            <div id="mrs-m" class="title-letter"></div>
            <div id="mrs-r" class="title-letter"></div>
            <div id="mrs-s" class="title-letter"></div>
    </section>
    
    <section id="intro">
       	<div class="ribbon">
        	<div class="ribbon-border">
                <div class="container">
                    <h1>Staying at Northbrook Park Mews</h1>
                    <p>Stumbling distance from the wedding venue are <span class="number">10</span> cottages that we have saved for our closest friends and family.</p>
                    <p>As the music has to be turned off by <span class="number">23:30</span> at the venue, having the cottages onsite means that the party can carry on for those who want it to - like us!</p>
                </div>
            </div>
        </div>
    </section>
    
    <section id="content">
    	<div class="container">
        	<h2>The Cottages</h2>
            <p>Each cottage sleeps eight people and includes two double rooms and a family room.</p>
            <p>All rooms have their own private bathroom and share a large kitchen, lounge and dining area where you can have breakfast in the morning from the hamper delivered to your door.</p>
            <p>Check in: <span class="number">13:00</span><br>
            Check out: <span class="number">10:00</span></p>
        </div>
    </section>
    
    <section id="pricing">
    	<div class="container">
        	<h2>Prices</h2>
            <table width="80%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td>Double Room
                	<em>Sleeps two</em></td>
                <td>&pound;<span class="number">110</span></td>
              </tr>
              <tr>
                <td>Family Room<br>
                	<em>One double bed and two single beds on a split mezzanine floor - sleeps four</em>
                </td>
                <td valign="middle">&pound;<span class="number">130</span></td>
              </tr>
              <tr>
                <td>Cottage<br>
					<em>Two double rooms and one family room - sleeps eight</em>
                </td>
                <td valign="middle">&pound;<span class="number">350</span></td>
              </tr>
            </table>
        </div>
    </section>
    
    <section id="booking">
    	<div class="container">
        	<h2>How To Book</h2>
            <p>By booking a full cottage and splitting the cost between eight, the price per person will be £<span class="number">43.75</span> (this include breakfast).</p>
            <p>To book a room/cottage, please contact the venue on <span class="number">0844 824 7771</span> by Friday <span class="number">23</span>rd April. After this date, we will allocate the bookings to specific cottages UNLESS you wish to arrange a group of eight/one full cottage yourself, which we're more than happy for you to do!</p>
            <p>Alternatively if you would like to book as part of a group of eight but need a few more people to make up the numbers, contact one of us and we will arrange this for you.</p>
            <p>Important information: You can learn more about the accommodation on <a href="http://www.northbrookparkmews.co.uk" target="_blank">www.northbrookparkmews.co.uk</a> but please DO NOT make your booking online as you will not be able to take advantage of our discounted rates. If there is still space among the cottages after the Friday <span class="number">23</span>rd April booking deadline, we will release the remaining spaces to our full wedding guest list.</p>
            <p>If you're unsure about anything here, contact us on <span class="number">07584 124636</span> (Lauren) or <span class="number">07850 024143</span> (Jack).</p>
        </div>
    </section>
    
    <footer>
    	Website by <a href="http://thetwentysix.co.uk/" target="_blank" title="Website by The Twenty Six">The Twenty Six</a>
    </footer>
        
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery-1.8.2.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.parallax-1.1.3.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/jquery.bxslider.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/enquire.min.js" type="text/javascript"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/scripts/wedding.js" type="text/javascript"></script>
<?php wp_footer() ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47915841-1', 'laurenandjacksbigday.co.uk');
  ga('send', 'pageview');

</script>
</body>
</html>